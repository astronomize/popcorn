package tokenizer;

public class Token {
    private TokenType type;
    private String token;
    
    public Token(TokenType type, String token) {
        this.type = type;
        this.token = token;
    }
    
    public TokenType getType() {
        return this.type;
    }
    
    public String getToken() {
        return this.token;
    }
}