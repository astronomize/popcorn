package tokenizer;


public enum TokenType {
    STRING, INTEGER, FUNCTION, COMMENT
}